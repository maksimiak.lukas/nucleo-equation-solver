# Nucleo Equation Solver

Equation solver is implemented in Nulceo-L476RG. It is used to solve simple linear and quadratic equations and always provides rational positive number as an answer. The format of an equation should be exactly: `Ax+Bx^2+x/C+D=0`. In this implementation Nucleo receives data with PA9 pin and transmits it through PA10 pin.


## Prerequisites

Make sure you have installed gcc: https://gcc.gnu.org/install/download.html
Also HAL libraries for Nucleo-L476RG are required.


## Usage

Connect NanoV3 which can send equations (or simply send equations yourself through UART) to Nucleo board, flash Nucleo with given configuration and Nucleo should solve the equations you provide. Sending eqaution should specify coefficients directly eq. `1*x-10=0`. Notice the '1' at the start of the equation.

## Comments

This equation solver is VERY limited as it only comes up with rational, positive solutions. If you feed UART with eqaution which does not have a positive rational root, Nucleo will simply round your answer, which is not desired behaviour.
