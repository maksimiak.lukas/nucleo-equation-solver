#include "main.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>

#define NO_OF_COEFFICIENTS 4
 
void LED_Init();
int *parse(char *msg)
{
    int A = 0, B = 0, C = 0, D = 0; //initial coefficients

    //perform lexical analysis on the input
    char num[10] = {}; //increased size to fit big D coef
    int c = 0;
    int positive = 1;
    for (int i = 0; i < strlen(msg); i++)
    {
        if (msg[i] == '='){
            break;
        }
        if (msg[i] == ' ' || msg[i] == '*'
            || msg[i] == '^' || msg[i] == '/' || msg[i] == '(' || msg[i] == ')')
        {
            continue;
        }
        if (msg[i] == '+')
        {
            positive = 1;
            continue;
        }
        if (msg[i] == '-')
        {
            positive = 0;
            continue;
        }

        if (msg[i] != 'x')
        {
            num[c++] = msg[i];
        }

        else {
            //check if it is squared
            if (msg[i+1] == '^')
            {
                B = atoi(num);
                if (!positive)
                    B *= -1;
                i += 2; //check this again
                memset(num, 0, sizeof num);
                c = 0;
            }
            //check if  it is followed by division
            else if (msg[i+1] == '/')
            {
                //now C is what follows after / until SPACE symbol

                //we create temporary char * to store C and then covert it to int
                char temp[2] = {0};
                int c_temp = 0;
                int k = i + 2;

                for (int j = k; j <= strlen(msg); j++){
                    if (msg[j] == '-' || msg[j] == '+' || msg[j] == '=')
                        break;
                    else
                        temp[c_temp++] = msg[j];
                }
                i += (c_temp + 1);
                C = atoi(temp);
                if (!positive)
                    C *= -1;
                memset(num, 0, sizeof num);
                c = 0;

            }
            else {
                A = atoi(num);
                if (!positive)
                    A *= -1;
                memset(num, 0, sizeof num);
                c = 0;
            }
        }

        D = atoi(num);
        if (!positive)
            D *= -1;

    }

    int *ret = malloc(NO_OF_COEFFICIENTS * sizeof(int));

    ret[0] = A;
    ret[1] = B;
    ret[2] = C;
    ret[3] = D;

    return ret;
}
int solve(int *coefficients)
{
    float solution = 0; //set arbitrary solution
    //assign the coefficients
    int A = coefficients[0];
    int B = coefficients[1];
    int C = coefficients[2];
    int D = coefficients[3];

    //add A and 1/C together
    float b = 0;

    if (C != 0){    //avoid division by zero
       b = A + (float)1/C;
    } else {
       b = A;
    }

    //check if it is a quadratic equation
    if (B != 0) {
        //if so, we can simply use quadratic formula

        //Bx^2 + bx + D = 0
        float DET = pow(b, 2.0) - 4 * B * D;
        float sol1 = ((-1 * b) + sqrt(DET)) / (2 * B);
        float sol2 = ((-1 * b) - sqrt(DET)) / (2 * B);

        //find positive, whole number solution
        if (sol1 < 0){
            solution = sol2;
        } else {
            solution = sol1;
        }

    } else {
        //this equation happens to be linear

        //bX + D = 0
        solution = (-1 * D) / b; //b is never zero
    }

    return solution;
}

void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};

  /* MSI is enabled after System reset, activate PLL with MSI as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.MSICalibrationValue = RCC_MSICALIBRATION_DEFAULT;

  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 40;
  RCC_OscInitStruct.PLL.PLLR = 2;
  RCC_OscInitStruct.PLL.PLLP = 7;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    /* Initialization Error */
    while(1);
  }
  
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;  
  if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    /* Initialization Error */
    while(1);
  }
}
static void Error_Handler(void)
{
  while(1)
  {
    /* Toggle LED for error */
    HAL_GPIO_TogglePin(LED_GPIO_PORT, LED_PIN);
    HAL_Delay(500);
  }
}
void HAL_UART_MspInit(UART_HandleTypeDef *huart)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  
  __GPIOA_CLK_ENABLE();
  __USART1_CLK_ENABLE();

  GPIO_InitStruct.Pin = GPIO_PIN_9 | GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP; 
  /*!< Peripheral to be connected to the selected pins. 
  This parameter can be a value of @ref GPIO_Alternate_function_selection */
  GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}
UART_HandleTypeDef UARTHandle;

void append(char* s, char c) {
        int len = strlen(s);
        s[len] = c;
        s[len+1] = '\0';
}
int main(void) {

  HAL_Init();
  SystemClock_Config();

  UARTHandle.Instance = USART1; 

  UARTHandle.Init.BaudRate   = 9600;
  UARTHandle.Init.WordLength = UART_WORDLENGTH_8B;
  UARTHandle.Init.StopBits   = UART_STOPBITS_1;
  UARTHandle.Init.Parity     = UART_PARITY_NONE;
  UARTHandle.Init.Mode       = UART_MODE_TX_RX;
  UARTHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
  UARTHandle.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;

  HAL_UART_MspInit(&UARTHandle);

  if(HAL_UART_DeInit(&UARTHandle) != HAL_OK)
  {
    Error_Handler();
  }  
  if(HAL_UART_Init(&UARTHandle) != HAL_OK)
  {
    Error_Handler();
  }

  LED_Init();
  
  while (1)
  {
    
    //receive information byte by byte and store it to recv_buff 
    char recv_buff[256] = {};
    for (;;){
      char *recv_c = malloc(sizeof(char));
      HAL_UART_Receive(&UARTHandle, (uint8_t*)recv_c, 1, HAL_MAX_DELAY);
  
      if (recv_c[0] == '\n'){
          break;
      }
        
      append(recv_buff, recv_c[0]);  
      
    }

    // array for storing coefficients
    int *store_array = malloc(NO_OF_COEFFICIENTS * sizeof(int));
    
    // parse buffer
    store_array = parse(recv_buff); 

    // solve equation
    int solution = solve(store_array);

    // send solution
    char buffer[256];  
    HAL_UART_Transmit(&UARTHandle, (uint8_t*)buffer, sprintf(buffer, "%d", solution), HAL_MAX_DELAY);
    
  }
}

void LED_Init() {
  LED_GPIO_CLK_ENABLE();
  GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitStruct.Pin = LED_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
  HAL_GPIO_Init(LED_GPIO_PORT, &GPIO_InitStruct);
}

void SysTick_Handler(void) {
  HAL_IncTick();
}
